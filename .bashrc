set -o vi
bind -m vi-insert "\C-l":clear-screen
bind -m vi-command "\C-l":clear-screen

# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

alias ll="ls -lha"
alias stauts="status"

# Get ip addresses
ipaddr() { (awk '{print $2}' <(ifconfig en0 | grep 'inet ')); }

# Appearance for CLI and ls
# export PS1='[\W]$ '
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

# Less Colors for Man Pages
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;38;5;208m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[04;38;5;111m'

# Set unlimiited history size
export HISTFILESIZE=
export HISTSIZE=


#TODO: Allow for different config root
# Machine specific settings go in here
[ ! -d ~/.cfg ] && \
    echo ".cfg directory not installed at canonical path '~/.cfg'" && \
    exit 1 

for f in ~/.cfg/machine-specific/*.sh; do 
    . $f
done


# Pretty-print path
function ppath() {
    old=$IFS
    IFS=:
    printf "%s\n" $PATH
    IFS=$old
}
export -f ppath

function pldpath() {
    old=$IFS
    IFS=:
    printf "%s\n" $LD_LIBRARY_PATH
    IFS=$old
}
export -f pldpath

# Rust
export PATH="$HOME/.cargo/bin:$PATH"

# Python
export PATH=:"${PYTHON_HOME}:${PATH}"
export PATH=${HOME}/.local/bin:$PATH


# Spark 
export PATH="${SPARK_HOME}/bin:${PATH}"
export PYSPARK_PYTHON=${PYTHON_HOME}
export PYSPARK_DRIVER_PYTHON=${PYSPARK_PYTHON}

function pyspark-notebook() {
  PYSPARK_DRIVER_PYTHON=${JUPYTER_HOME} \
  PYSPARK_DRIVER_PYTHON_OPTS="notebook --NotebookApp.token=''" \
  pyspark "$@"
}
export -f pyspark-notebook

function pyspark-console() {
  PYSPARK_DRIVER_PYTHON=${JUPYTER_HOME} \
  PYSPARK_DRIVER_PYTHON_OPTS="console" \
  pyspark "$@"
}
export -f pyspark-console

function pyspark-submit() {
  PYSPARK_DRIVER_PYTHON=${PYSPARK_PYTHON} \
  PYSPARK_DRIVER_PYTHON_OPTS=" " \
  ${SPARK_HOME}/bin/spark-submit "$@"
}
export -f pyspark-submit

#export ARROW_PRE_0_15_IPC_FORMAT=1

export PKG_CONFIG_PATH="/usr/local/opt/qt/lib/pkgconfig"
export LDFLAGS=-L/usr/local/opt/qt/lib
export CPPFLAGS=-I/usr/local/opt/qt/include

eval "$(pyenv init -)"

function get_abs_filename() {
  # $1 : relative filename
  if [ -d "$(dirname "$1")" ]; then
    echo "$(cd "$(dirname "$1")" && pwd)/$(basename "$1")"
  fi
}
export -f get_abs_filename

function cprep() {
  cp report.pdf report-$(git rev-parse HEAD).pdf
}
export -f cprep 
